Shopping Basket API for Bigpoint
----------------------------------

### Database model
![alt text](img/database_model.png)

### API documentation
[API docs on postman.co](https://web.postman.co/collections/23846-793da242-3c9f-4122-becf-98134b345b6d?version=latest&workspace=7ce5bc5b-57ca-4091-880b-542c29f98c82)

### How to start (local)
To just start the API do the following commands.
This will start the API with a local sqlite database.
``` bash
git clone git@gitlab.com:reetz.jonas/shopping_basket.git
pipenv sync
pipenv run python run.py
```

### How to start (docker)
To start the API in a docker container do the following commands.
``` bash
docker build -t shopping_basket:latest .
docker container run -d -p 80:80 shopping_cart:latest
```

### How to start (docker-compose)
To start the API with a postgresql database in the backend do the following
``` bash
docker build -t shopping_basket:latest .
docker-compose -f docker-compose-postgresql.yml up --build
```

### How the CI should be implemented
- execute code tests
  * unit tests for the python code
  * check source code for vulnerabilities e.g. [bandit](https://github.com/PyCQA/bandit)
  * check libraries for vulnerabilities e.g. [pipenv check](https://docs.pipenv.org/en/latest/#pipenv-check)
- build docker images
- push docker images to a registry
- deploy new images to a test environment (kubernetes or docker swarm)
- test deployed API
  * with a populated database
  * automated API testing e.g. [SoapUI](https://www.soapui.org/)
  * check containers for vulnerabilities e.g. [clair](https://github.com/coreos/clair), [clair-scanner](https://github.com/arminc/clair-scanner) 
- deploy new images to production environment (kubernetes or docker swarm)

#### What to improve / thoughts for the next time
I am totally unhappy with the choice of sqlalchemy in ORM mode. I rather use plain queries next time as I know how to do it.

sqlalchemy ORM was a quick shot as I shortly used it before in a small project but it is to large to overview in the short time.