import falcon
import falcon.testing

import app
from app.models import Basket, Customer, Product


class MyTestCase(falcon.testing.TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()
        self.app, self.db_session = app.create("sqlite:///:memory:")

        self.api_token = "test"
        self.auth_headers = {"Authorization": f"Token {self.api_token}"}

        self.prepare_database()

    def prepare_database(self):
        test_customer = Customer(user_name="Test", api_token=self.api_token)
        self.db_session.add(test_customer)
        self.db_session.commit()
        self.db_session.refresh(test_customer)

        test_basket = Basket(customer_id=test_customer.customer_id, checked_out=False)
        self.db_session.add(test_basket)

        product = Product(name="Mate Tee")
        self.db_session.add(product)

        self.db_session.commit()


class TestBasket(MyTestCase):
    def test_get_baskets(self):
        required_result = [{'basket_id': 1, 'checked_out': False, 'customer_id': 1}]

        result = self.simulate_get('/baskets', headers={"Authorization": "Token test"})
        self.assertEqual(result.json, required_result)
        self.assertEqual(result.status, falcon.HTTP_200)

    def test_add_product_to_baskets(self):
        result = self.simulate_put(
            '/baskets/1/products',
            body="[1]",
            headers=self.auth_headers
        )
        self.assertEqual(result.status, falcon.HTTP_200)
