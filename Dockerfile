FROM python:3.7-alpine AS build

# install build deps for postgresql
RUN apk update && apk add postgresql-dev gcc musl-dev

# Copy pipenv files early to make use of docker caching, if they get changed the build takes much longer
COPY Pipfile /shopping_basket/Pipfile
COPY Pipfile.lock /shopping_basket/Pipfile.lock

WORKDIR /shopping_basket

RUN pip install pipenv
RUN pipenv sync

# create new stage to get rid of the build mess
FROM python:3.7-alpine
# install postgresql libs
RUN apk update && apk add libpq
EXPOSE 80
# this might be used if db runs on sqlite
VOLUME /shopping_basket/db

RUN pip install pipenv

# copy virtualenv from build stage
COPY --from=build /root/.local/share/virtualenvs/ /root/.local/share/virtualenvs/

# copy rest of the files to current stage
COPY . /shopping_basket
WORKDIR /shopping_basket

# start gunicorn
CMD ["pipenv", "run", "gunicorn", "-b", "0.0.0.0:80", "run_external:api"]