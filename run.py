from wsgiref.simple_server import make_server

from app import create


if __name__ == '__main__':
    api, db_session = create("sqlite:///test.db")

    with make_server('', 8000, api) as httpd:
        print('Serving on port 8000...')

        # Serve until process is killed
        httpd.serve_forever()