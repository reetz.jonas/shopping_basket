import falcon
from ..models.base import Base


class BaseCollectionResource:
    model = Base
    max_recursion_depth = 0
    join_tables = tuple()

    def query_auth_callback(self, query, req: falcon.Request):
        return query

    def on_get(self, req: falcon.Request, resp: falcon.Response, **filter_by_args):
        query = self.session.query(self.model)

        for table in self.join_tables:
            query = query.join(table)

        query = self.query_auth_callback(query, req)

        results = query.filter_by(**filter_by_args).all()

        resp.media = [entry.to_dict() for entry in results]


class BaseResource:
    model = Base
    max_recursion_depth = 0

    def query_auth_callback(self, query, req):
        return query

    def on_get(self, req: falcon.Request, resp: falcon.Response, **filter_by_args):
        query = self.session.query(self.model).filter_by(**filter_by_args)

        query = self.query_auth_callback(query, req)

        entry = query.first()

        if not entry:
            resp.status = falcon.HTTP_NOT_FOUND
            return

        resp.media = entry.to_dict()
