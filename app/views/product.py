import falcon
import sqlalchemy.exc

from app.models import Product
from .base import BaseCollectionResource, BaseResource


class ProductCollectionResource(BaseCollectionResource):
    auth = {
        'exempt_methods': ['GET', 'POST']
    }

    model = Product
    join_tables = (Product.baskets,)

    def on_post(self, req: falcon.Request, resp: falcon.Response, **filter_by_args):
        try:
            new_product = Product(name=req.media["name"])
        except (KeyError, TypeError):
            resp.media = {"error": 'Wrong input format. Use this: {"name": "PRODUCT_NAME"}'}
            resp.status = falcon.HTTP_UNPROCESSABLE_ENTITY
            return

        self.session.add(new_product)
        try:
            self.session.commit()
        except sqlalchemy.exc.IntegrityError:
            resp.media = {"error": "Duplicate product name"}
            resp.status = falcon.HTTP_UNPROCESSABLE_ENTITY
            return

        self.session.refresh(new_product)
        self.session.commit()

        resp.media = new_product.to_dict()


class ProductResource(BaseResource):
    auth = {
        'exempt_methods': ['GET']
    }

    model = Product
