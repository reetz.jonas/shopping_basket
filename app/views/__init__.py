from .basket import BasketResource, BasketCollectionResource, BasketProductsCollectionResource
from .customer import CustomerResource, CustomerCollectionResource
from .product import ProductCollectionResource, ProductResource
