import secrets

import falcon
import sqlalchemy.exc

from app.models import Customer, Basket
from .base import BaseCollectionResource, BaseResource


def customer_auth_check(self, query, req):
    return query.filter_by(customer_id=req.context['user'].customer_id)


class CustomerCollectionResource(BaseCollectionResource):
    model = Customer
    query_auth_callback = customer_auth_check

    auth = {
        'exempt_methods': ["POST"]
    }

    def on_post(self, req: falcon.Request, resp: falcon.Response, **filter_by_args):
        try:
            new_customer = Customer(
                user_name=req.media["user_name"],
                api_token=secrets.token_urlsafe(32)
            )
        except (KeyError, TypeError):
            resp.media = {"error": 'Wrong input format. Use this: {"user_name": "USERNAME"}'}
            resp.status = falcon.HTTP_UNPROCESSABLE_ENTITY
            return

        self.session.add(new_customer)
        try:
            self.session.commit()
        except sqlalchemy.exc.IntegrityError:
            resp.media = {"error": "Duplicate username"}
            resp.status = falcon.HTTP_UNPROCESSABLE_ENTITY
            return

        self.session.refresh(new_customer)

        user_basket = Basket(customer_id=new_customer.customer_id)
        self.session.add(user_basket)
        self.session.commit()

        resp.media = new_customer.to_dict()


class CustomerResource(BaseResource):
    model = Customer
    query_auth_callback = customer_auth_check
