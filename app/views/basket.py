import falcon

from app.models import Basket, Product
from app.models.basket import BasketProduct
from .base import BaseCollectionResource, BaseResource


def basket_auth_check(self, query, req):
    return query.filter_by(customer_id=req.context['user'].customer_id)


class BasketCollectionResource(BaseCollectionResource):
    model = Basket
    query_auth_callback = basket_auth_check


class BasketResource(BaseResource):
    model = Basket
    query_auth_callback = basket_auth_check


class BasketProductsCollectionResource:
    query_auth_callback = basket_auth_check

    def on_get(self, req: falcon.Request, resp: falcon.Response, **filter_by_args):
        query = self.session.query(BasketProduct).filter_by(**filter_by_args).join(Basket)
        query = self.query_auth_callback(query, req)

        results = query.all()

        resp.media = [entry.to_dict(max_nesting=1) for entry in [prod.product for prod in results]]

    def on_put(self, req: falcon.Request, resp: falcon.Response, **filter_by_args):
        if not isinstance(req.media, list):
            resp.status = falcon.HTTP_UNPROCESSABLE_ENTITY
            return

        query = self.session.query(Basket).filter_by(**filter_by_args)
        query = self.query_auth_callback(query, req)
        basket = query.first()

        if not basket:
            resp.status = falcon.HTTP_NOT_FOUND
            return

        # get all requested product_ids which are in the db
        new_products = self.session.query(Product).filter(Product.product_id.in_(req.media)).all()
        available_product_ids = [prod_id for prod_id in req.media if prod_id in [p.product_id for p in new_products]]

        # add products to the basket
        new_bp = []
        for product_id in available_product_ids:
            bp = BasketProduct(basket_id=basket.basket_id, product_id=product_id)
            new_bp.append(bp)
            self.session.add(bp)

        self.session.commit()
        self.session.refresh(basket)

        resp.media = [bp.to_dict() for bp in new_bp]
