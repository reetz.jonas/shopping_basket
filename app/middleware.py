class SQLAlchemySessionManager:
    """
    This class is to get access to a sqlalchemy session in all falcon request classes
    Got this from https://eshlox.net/2017/07/28/integrate-sqlalchemy-with-falcon-framework

    """

    def __init__(self, session):
        self.session = session

    def process_resource(self, req, resp, resource, params):
        resource.session = self.session()

    def process_response(self, req, resp, resource, req_succeeded):
        if hasattr(resource, 'session'):
            if not req_succeeded:
                resource.session.rollback()
            self.session.remove()