from sqlalchemy import Integer, Text
from sqlathanor import Column, relationship

from .base import Base


class Product(Base):
    __tablename__ = "products"
    product_id = Column(Integer, primary_key=True, supports_dict=True)

    name = Column(Text, nullable=False, supports_dict=True, unique=True)
    baskets = relationship('BasketProduct')
