from sqlalchemy import String, Integer
from sqlathanor import Column, relationship

from .base import Base


class Customer(Base):
    __tablename__ = "customers"

    customer_id = Column(Integer, primary_key=True, supports_dict=True)
    user_name = Column(String(20), nullable=False, supports_dict=True, unique=True)
    api_token = Column(String(255), nullable=False, supports_dict=True)

    baskets = relationship("Basket")
