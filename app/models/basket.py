from sqlalchemy import Integer, ForeignKey, Boolean, UniqueConstraint
from sqlathanor import Column, relationship

from .base import Base


class BasketProduct(Base):
    __tablename__ = "baskets_products"

    basket_product_id = Column(Integer, primary_key=True, supports_dict=True)
    basket_id = Column(Integer, ForeignKey('baskets.basket_id'), supports_dict=True)
    product_id = Column(Integer, ForeignKey('products.product_id'), supports_dict=True)
    basket = relationship("Basket", uselist=False, supports_dict=True)
    product = relationship("Product", uselist=False, supports_dict=True)

    UniqueConstraint('basket_id', 'product_id', name='uix_1')


class Basket(Base):
    __tablename__ = "baskets"
    basket_id = Column(Integer, primary_key=True, supports_dict=True)
    checked_out = Column(Boolean, nullable=False, default=False, supports_dict=True)
    customer_id = Column(Integer, ForeignKey('customers.customer_id'), supports_dict=True)

    products = relationship('BasketProduct', supports_dict=True)
