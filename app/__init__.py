import falcon
from falcon_auth import FalconAuthMiddleware, TokenAuthBackend
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from app import views
from app.middleware import SQLAlchemySessionManager
from app.models import Base, Customer


def create(database_url, echo_database=False):
    # initialize database
    engine = create_engine(database_url, echo=echo_database)

    # sqlalchemy
    session_factory = sessionmaker(bind=engine)
    Session = scoped_session(session_factory)

    def token_user_loader(token):
        session = Session()
        loaded_user = session.query(Customer).filter_by(api_token=token).first()
        return loaded_user

    # auth
    token_auth = TokenAuthBackend(token_user_loader)

    api = falcon.API(middleware=[
        SQLAlchemySessionManager(Session),
        FalconAuthMiddleware(token_auth)
    ])

    Base.metadata.create_all(engine)

    api.add_route("/baskets", views.BasketCollectionResource())
    api.add_route("/baskets/{basket_id}", views.BasketResource())
    api.add_route("/baskets/{basket_id}/products", views.BasketProductsCollectionResource())

    api.add_route("/products", views.ProductCollectionResource())
    api.add_route("/products/{product_id}", views.ProductResource())

    api.add_route("/customers", views.CustomerCollectionResource())
    api.add_route("/customers/{customer_id}", views.CustomerResource())
    api.add_route("/customers/{customer_id}/baskets", views.BasketCollectionResource())

    return api, Session
