import os

from app import create

db_path = os.path.join(os.path.dirname(__file__), "db", "basket.db")
db_uri = os.environ.get("DB_URI") or f'sqlite:///{db_path}'
api, db_session = create(db_uri, echo_database=True)
